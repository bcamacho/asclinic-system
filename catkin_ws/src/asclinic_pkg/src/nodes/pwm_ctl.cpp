#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt32.h"

int pwm=0;
ros::Rate loop_rate(20)

// Respond to subscriber receiving a message
void templateSubscriberCallback(const std_msgs::UInt32& msg)
{
	if(msg==1)
	{
		pwm=0;
	}
	else
	{
		pwm=50
	}
	ROS_INFO_STREAM("[TEMPLATE CPP NODE MINIMAL] Message receieved with data = " << msg.data);
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "template_cpp_node_minimal");
	ros::NodeHandle nodeHandle("~");
	// Initialise a publisher
	ros::Publisher pwm_publisher = nodeHandle.advertise<std_msgs::UInt32>("duty_cycle", 10, false);
	// Initialise a subscriber
	ros::Subscriber collision_subscriber = nodeHandle.subscribe("gpio_approx", 1, SubscriberCallback);
	
	while(ros::ok())
	{
		ros::spinOnce();
		std_msgs::Int32 msg;
		msg.data = speed;
		pwm_publisher.publish(msg);
		loop_rate.sleep();
	}
	// Spin as a single-threaded node
	ros::spin();

	return 0;
}
